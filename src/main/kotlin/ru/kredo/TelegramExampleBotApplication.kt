package ru.kredo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("ru.kredo.bot")
class TelegramExampleBotApplication

fun main(args: Array<String>) {
	runApplication<TelegramExampleBotApplication>(*args)
}
