package ru.kredo.bot

import database.*
import io.github.jan.supabase.postgrest.from
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns
import io.github.jan.supabase.postgrest.rpc
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.regex.Pattern


@Service
class TelegramBotExample(
    @Value("\${telegram.botName}") private val botName: String,
    @Value("\${telegram.botToken}") private val botToken: String
) : TelegramLongPollingBot(botToken) {

    @PostConstruct
    fun init() {
        println("init")
        TelegramBotsApi(DefaultBotSession::class.java).registerBot(this)
    }

    override fun getBotUsername(): String = botName

    private var idCoworking : Int = 0
    private var idService : Int = 0
    private var dateBooking : String = ""
    private var timeBoking : String = ""
    private var phoneUser : String = ""

    override fun onUpdateReceived(update: Update) {
        if (update.hasMessage() && update.message.hasText()) {
            val chatId = update.message.chatId
            val text = update.message.text
            when (text) {
                "/start" -> sendKeyboardMessageButton(chatId)
                "Бронирование" -> seatReservationButton(chatId)
                "Информация" -> infoButton(chatId)
                "Вернуться на главное меню" -> sendKeyboardMessageButton(chatId)
                "Новости" -> infoButton(chatId)
                "Кредо в Шлиссельбурге" -> GlobalScope.launch { getCoworkingById(chatId, 1) }
                "Кредо в Кировске" -> GlobalScope.launch { getCoworkingById(chatId, 2) }
                "События в этом месяце" -> GlobalScope.launch { getEvents(chatId, "get_events_this_month") }
                "События на этой неделе" -> GlobalScope.launch { getEvents(chatId, "get_events_this_week") }
                "В Кировске" ->  GlobalScope.launch { idCoworking = 2; sendServiceOptions(chatId, 2) }
                "В Шлиссельбурге" -> GlobalScope.launch { idCoworking = 1; sendServiceOptions(chatId, 1) }
                "Вернуться к выбору коворкинга" -> seatReservationButton(chatId)
                "Дартс" -> GlobalScope.launch {idService = 3; sendDateSelection(chatId) }
                "Настольный теннис" -> GlobalScope.launch {idService = 1; sendDateSelection(chatId)}
                "Рабочее место в общем зале с ноутбуком" -> GlobalScope.launch {idService = 2; sendDateSelection(chatId)}
                "Настольные игры" -> GlobalScope.launch {idService = 4; sendDateSelection(chatId)}
                "Переговорная комната" -> GlobalScope.launch {idService = 5; sendDateSelection(chatId)}
                else -> handleText(chatId, text)
            }
        }
    }

    private fun handleText(chatId: Long, text: String) {
        when {
            isValidDate(text) -> {
                dateBooking = text
                sendTimeMessage(chatId)
            }
            isValidTime(text) -> {
                timeBoking = text
                handleValidTime(chatId, text)
                sendPhoneMessage(chatId)
            }
            isValidPhone(text) -> {
                phoneUser = text
                handleValidPhone(chatId, text)
                GlobalScope.launch { addBooking() }
            }
            else -> {
                sendInvalidMessage(chatId)
            }
        }
    }
    private fun isValidPhone(text: String): Boolean {
        val phonePattern = Pattern.compile("^\\+\\d{11}$")
        return phonePattern.matcher(text).matches()
    }

    private fun handleValidPhone(chatId: Long, phone: String) {
        sendMessage(chatId, "Номер телефона: $phone успешно установлен.")
    }
    private fun sendPhoneMessage(chatId: Long){
        sendMessage(chatId, "Введите номер телефона в формате +7XXXXXXXXXX")
    }

    private fun isValidDate(text: String): Boolean {
        return try {
            val inputDate = LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
            !inputDate.isBefore(LocalDate.now())
        } catch (e: Exception) {
            false
        }
    }
    private fun isValidTime(text: String): Boolean {
        return try {
            val inputTime = LocalTime.parse(text, DateTimeFormatter.ofPattern("HH:mm:ss"))
            val startTime = LocalTime.of(11, 0, 0)
            val endTime = LocalTime.of(19, 0, 0)
            !inputTime.isBefore(startTime) && !inputTime.isAfter(endTime)
        } catch (e: Exception) {
            false
        }
    }
    private fun handleValidTime(chatId: Long, time: String) {
        sendMessage(chatId, "Время бронирования: $time успешно установлено.")
    }

    private fun sendInvalidTimeMessage(chatId: Long) {
        sendMessage(chatId, "Введенные данные некорректны. Пожалуйста, введите время корректно.")
    }


    private fun sendMessage(chatId: Long, text: String, replyMarkup: ReplyKeyboardMarkup? = null) {
        val sendMessage = SendMessage().apply {
            this.chatId = chatId.toString()
            this.text = text
            this.replyMarkup = replyMarkup
        }
        try {
            execute(sendMessage)
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    private suspend fun addBooking(){
        val client = NetworkModule.provideSupabaseClient()
        val booking = Bookings(
            phone = phoneUser,
            booking_date = LocalDate.parse(dateBooking),
            booking_time = LocalTime.parse(timeBoking),
            request_datetime = LocalDateTime.now(),
            id_service = idService,
            id_coworking = idCoworking)
        client.from("public", "bookings").insert(booking)
    }

    private fun createDateSelectionKeyboard(): ReplyKeyboardMarkup {
        val keyboardMarkup = ReplyKeyboardMarkup()
        val keyboard = mutableListOf<KeyboardRow>()
        val today = LocalDate.now()
        val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        for (i in 0..6) {
            val date = today.plusDays(i.toLong())
            val row = KeyboardRow()
            row.add(KeyboardButton(date.format(dateFormatter)))
            keyboard.add(row)
        }
        keyboard.add(KeyboardRow().apply { add(KeyboardButton("Вернуться к выбору коворкинга")) })
        keyboardMarkup.keyboard = keyboard
        keyboardMarkup.resizeKeyboard = true
        keyboardMarkup.oneTimeKeyboard = true
        return keyboardMarkup
    }

    suspend fun sendDateSelection(chatId: Long) {
        val sendMessage = SendMessage()
        sendMessage.chatId = chatId.toString()
        sendMessage.text = "Выберите дату:"
        val keyboardMarkup = createDateSelectionKeyboard()
        sendMessage.replyMarkup = keyboardMarkup
        execute(sendMessage)
    }

    private fun sendInvalidMessage(chatId: Long) {
        sendMessage(chatId, "Данные введены некорректно!")
    }

    private fun startKeyboard(): ReplyKeyboardMarkup {
        return createKeyboard(listOf("Новости", "Информация", "Бронирование", "Чат с поддержкой"))
    }

    private fun sendKeyboardMessageButton(chatId: Long) {
        sendMessage(chatId, "Что вас интересует?", startKeyboard())
    }
    private fun sendTimeMessage(chatId: Long){
        sendMessage(chatId, "Введите время на которое хотите забронировать. Ваше время должно входить в диапазан с 11:00 до 19:00. Пример для ввода 17:00:00")
    }

    private suspend fun getEvents(chatId: Long, rpcName: String) {
        val events = getEventInfo(rpcName)
        events?.forEach { event ->
            val caption = """
                |Мероприятие:
                |Дата: ${event.date_event}
                |Название: ${event.title_event}
                |Описание: ${event.description}
                |Местоположение: ${event.location}
            """.trimMargin()
            val mediaList = event.image_urls.mapIndexed { index, url ->
                InputMediaPhoto().apply {
                    media = url
                    if (index == 0) {
                        this.caption = caption
                    }
                }
            }
            val mediaGroup = SendMediaGroup().apply {
                this.chatId = chatId.toString()
                this.medias = mediaList
            }
            execute(mediaGroup)
        }
    }

    private suspend fun getEventInfo(rpcName: String): List<EventsModel>? {
        val client = NetworkModule.provideSupabaseClient()
        val json = client.postgrest.rpc(rpcName).data
        val jsonConfig = Json { ignoreUnknownKeys = true }
        return json?.let { jsonConfig.decodeFromString<List<EventsModel>>(it) }
    }

    private fun createKeyboard(buttons: List<String>): ReplyKeyboardMarkup {
        val keyboardMarkup = ReplyKeyboardMarkup()
        val keyboard = buttons.map { title ->
            KeyboardRow().apply { add(KeyboardButton(title)) }
        }
        keyboardMarkup.keyboard = keyboard
        keyboardMarkup.resizeKeyboard = true
        keyboardMarkup.oneTimeKeyboard = true
        return keyboardMarkup
    }

    private fun infoButton(chatId: Long) {
        sendMessage(chatId, "Какая информация вас интересует?", createKeyboard(listOf("Кредо в Шлиссельбурге", "Кредо в Кировске", "Вернуться на главное меню")))
    }

    private suspend fun getCoworkingById(chatId: Long, id: Int) {
        val coworking = getCoworkingInfo(id)
        val sendMessage = SendMessage().apply {
            this.chatId = chatId.toString()
            text = if (id == 1) {
                "Кредо в Шлиссельбурге:\nМы находимся по адресу: ${coworking?.location},\nНаш номер телефона: ${coworking?.phone},\nНаша страница в ВК, чтобы ничего не пропускать: ${coworking?.vk_link},\nРады вас видеть ${coworking?.time_work}"
            } else {
                "Кредо в Кировске:\nМы находимся по адресу: ${coworking?.location},\nНаш номер телефона: ${coworking?.phone},\nНаша страница в ВК, чтобы ничего не пропускать: ${coworking?.vk_link},\nРады вас видеть ${coworking?.time_work}"
            }
        }
        execute(sendMessage)
    }

    private suspend fun getCoworkingInfo(id: Int): CoworkingModel? {
        val client = NetworkModule.provideSupabaseClient()
        val json = client.from("coworking").select(
            columns = Columns.list("id_coworking", "location", "phone", "vk_link", "time_work")
        ) { filter { eq("id_coworking", id) } }.data
        val jsonConfig = Json { ignoreUnknownKeys = true }
        return json?.let { jsonConfig.decodeFromString<List<CoworkingModel>>(it).firstOrNull() }
    }

    private fun seatReservationButton(chatId: Long) {
        sendMessage(chatId, "В каком коворкинге вы хотите забронировать?", createKeyboard(listOf("В Кировске", "В Шлиссельбурге", "Вернуться на главное меню")))
    }

    private suspend fun getServicesByCoworking(coworkingId: Int): List<ServiceModel>? {
        val client = NetworkModule.provideSupabaseClient()
        val json = client.postgrest.rpc("get_services_by_coworking", mapOf("coworking_id" to coworkingId)).data
        val jsonConfig = Json { ignoreUnknownKeys = true }
        return json?.let { jsonConfig.decodeFromString<List<ServiceModel>>(it) }
    }

    suspend fun sendServiceOptions(chatId: Long, coworkingId: Int) {
        val services = getServicesByCoworking(coworkingId)
        val sendMessage = SendMessage().apply {
            this.chatId = chatId.toString()
            text = if (services != null && services.isNotEmpty()) {
                "Выберите сервис:"
            } else {
                "В выбранном коворкинге нет доступных сервисов."
            }
            replyMarkup = createServiceKeyboard(services?.map { it.title_service } ?: emptyList())
        }
        execute(sendMessage)
    }

    private fun createServiceKeyboard(serviceTitles: List<String>): ReplyKeyboardMarkup {
        val keyboardMarkup = ReplyKeyboardMarkup()
        val keyboard = serviceTitles.map { title ->
            KeyboardRow().apply { add(KeyboardButton(title)) }
        }.toMutableList().apply {
            add(KeyboardRow().apply { add(KeyboardButton("Вернуться к выбору коворкинга")) })
        }
        keyboardMarkup.keyboard = keyboard
        keyboardMarkup.oneTimeKeyboard = true
        keyboardMarkup.resizeKeyboard = true
        return keyboardMarkup
    }
}