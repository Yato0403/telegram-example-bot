package database


import ru.kredo.serializeData.LocalDateSerializer
import ru.kredo.serializeData.LocalTimeSerializer
import ru.kredo.serializeData.LocalDateTimeSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@kotlinx.serialization.Serializable
data class Bookings(
    val id_service: Int,
    val id_coworking: Int,
    @Serializable(with = LocalDateSerializer::class)
    val booking_date: LocalDate,
    @Serializable(with = LocalTimeSerializer::class)
    val booking_time: LocalTime,
    @Serializable(with = LocalDateTimeSerializer::class)
    val request_datetime : LocalDateTime,
    val phone: String
)
