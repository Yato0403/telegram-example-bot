package database
@kotlinx.serialization.Serializable
data class EventsModel(
    val id_event : Int,
    val title_event : String,
    val date_event : String,
    val description : String,
    val location : String,
    val image_urls: List<String>
)
