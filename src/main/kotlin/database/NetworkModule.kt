package database

import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.postgrest.Postgrest

object NetworkModule {
    fun provideSupabaseClient(): SupabaseClient {
        return createSupabaseClient(
            supabaseUrl = "https://wtmtmqdnbchcuktgyenw.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Ind0bXRtcWRuYmNoY3VrdGd5ZW53Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTY0OTg0OTgsImV4cCI6MjAzMjA3NDQ5OH0.OEybj2XduB9s1cGHE2BSqo7UjU-6Q3p4PzOqjU2iDkk"
        ){
            install(Auth)
            install(Postgrest)
        }
    }
}