package database

@kotlinx.serialization.Serializable
data class ServiceModel(
    val id_service : Int,
    val title_service : String,
    val description : String,
    val cost : Double
)
