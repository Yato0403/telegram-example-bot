package database

@kotlinx.serialization.Serializable
data class CoworkingModel(
    val id_coworking : Int,
    val location: String,
    val phone : String,
    val vk_link : String,
    val time_work : String
)
